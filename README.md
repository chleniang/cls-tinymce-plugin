# cls-tinymce-plugin

### 介绍
自定义tinymce-plugin，目前有段前间距、段后间距；

### 插件说明

#### 1. 段前间距
a.安装
- 跟其他tinymce插件安装方法一样，将clspspacebefore目录整个放到tinymce的plugins目录即可。
- 插件名称说明 cls:前缀 p:paragraph简写 space:间距 before:段前

b.使用说明
 - 插件列表中启用 clspspacebefore
 - 工具栏添加 clspspacebefore
 - 间距值列表自定义 p_space_before_val:"0 5px 10px 15px 20px 30px 40px"

```
    tinymce.init({
        ...
        plugins: "clspspacebefore",
        toolbar: "clspspacebefore",
        // 间距值列表自定义(有默认列表)
        p_space_before_val:"0 5px 10px 15px 20px 30px 40px",
        ...
    })
```

#### 2. 段后间距
a.安装
- 跟其他tinymce插件安装方法一样，将clspspaceend目录整个放到tinymce的plugins目录即可。
- 插件名称说明 cls:前缀 p:paragraph简写 space:间距 end:段后

b.使用说明
 - 插件列表中启用 clspspaceend
 - 工具栏添加 clspspaceend
 - 间距值列表自定义 p_space_end_val:"0 5px 10px 15px 20px 30px 40px"

```
    tinymce.init({
        ...
        plugins: "clspspaceend",
        toolbar: "clspspaceend",
        // 间距值列表自定义(有默认列表)
        p_space_end_val: "0 5px 10px 15px 20px 30px 40px",
        ...
    })
```


#### 特别鸣谢
---
** 段前间距/段后间距两插件均参照 five 大佬的 lineheight 插件修改而来,如有不到位之处请自行修改.

five的tinymce-plugin插件仓库 [https://gitee.com/fivecc/tinymce-plugins](https://gitee.com/fivecc/tinymce-plugins)

tinymce中文网插件制作教程 [http://tinymce.ax-z.cn/advanced/creating-a-plugin.php](http://tinymce.ax-z.cn/advanced/creating-a-plugin.php)

