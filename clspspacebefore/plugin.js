/**
 * clspspaceend 1.0
 * The tinymce-plugins is used to set the paragraph spacing before
 * Modified from Five(Li Hailong)'s lineheight plugin
 *    https://gitee.com/fivecc/tinymce-plugins
 *
 * Copyright 2021, cls-admin https://gitee.com/cls-admin
 * 
 * Licensed under MIT
 */

tinymce.PluginManager.add("clspspacebefore", function (editor, url) {
  var pluginName = "段前间距";
  var global$1 = tinymce.util.Tools.resolve("tinymce.util.Tools");
  var p_space_before_val = editor.getParam(
    "p_space_before_val",
    "0 5px 10px 15px 20px 25px 30px"
  );

  editor.on("init", function () {
    editor.formatter.register({
      margintop: {
        selector: "p,div,h1,h2,h3,h4,h5,h6,ul,ol,table",
        styles: { "margin-top": "%value" },
      },
    });
  });
  var doAct = function (value) {
    editor.undoManager.transact(function () {
      editor.focus();
      editor.formatter.apply("margintop", { value: value });
    });
  };
  editor.ui.registry.getAll().icons.pspacebefore ||
    editor.ui.registry.addIcon(
      "pspacebefore",
      '<svg viewBox="0 0 1024 1024" width="18" height="18"><path d="M0 433.152v-102.4h1024v102.4z m0 307.2v-102.4h1024v102.4z m0 281.6v-102.4h1024v102.4z m1024 0M512.512 0.256l-77.056 95.744-76.8 96h307.456l-76.8-96-76.8-95.744z"></path></svg>'
    );

  editor.ui.registry.addMenuButton("clspspacebefore", {
    icon: "pspacebefore",
    tooltip: pluginName,
    fetch: function (callback) {
      var dom = editor.dom;
      var blocks = editor.selection.getSelectedBlocks();
      var lhv = 0;
      global$1.each(blocks, function (block) {
        if (lhv == 0) {
          lhv = dom.getStyle(block, "margin-top")
            ? dom.getStyle(block, "margin-top")
            : 0;
        }
      });
      var items = p_space_before_val.split(" ").map(function (item) {
        var text = item;
        var value = item;
        return {
          type: "togglemenuitem",
          text: text,
          active: lhv == value ? true : false,
          onAction: function () {
            doAct(value);
          },
        };
      });
      callback(items);
    },
  });
  return {
    getMetadata: function () {
      return {
        name: pluginName,
        url: "https://gitee.com/cls-admin/cls-tinymce-plugin",
      };
    },
  };
});
