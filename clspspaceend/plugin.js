/**
 * clspspaceend 1.0
 * The tinymce-plugins is used to set the paragraph spacing end
 * Modified from Five(Li Hailong)'s lineheight plugin
 *    https://gitee.com/fivecc/tinymce-plugins
 *
 * Copyright 2021, cls-admin https://gitee.com/cls-admin
 * 
 * Licensed under MIT
 */

tinymce.PluginManager.add('clspspaceend', function(editor, url) {
    var pluginName='段后间距';
    var global$1 = tinymce.util.Tools.resolve('tinymce.util.Tools');
    var p_space_end_val = editor.getParam('p_space_end_val', '0 5px 10px 15px 20px 25px 30px');
  
    editor.on('init', function() {
        editor.formatter.register({
          marginbottom: {
                selector: 'p,div,h1,h2,h3,h4,h5,h6,ul,ol,table',
                styles: { 'margin-bottom': '%value' }
            }
        });
    });
    var doAct = function (value) {
        editor.undoManager.transact(function(){
          editor.focus();
          editor.formatter.apply('marginbottom', { value: value });
       })
    };
    editor.ui.registry.getAll().icons.pspaceend || editor.ui.registry.addIcon('pspaceend','<svg viewBox="0 0 1024 1024" width="18" height="18"><path d="M1024 588.8v102.4H0v-102.4z m0-307.2v102.4H0v-102.4z m0-281.6v102.4H0V0z m0 0M512 1021.952l-76.8-96-77.056-96h307.712L588.8 925.952l-76.8 96z"></path></svg>');
    
    editor.ui.registry.addMenuButton('clspspaceend', {
        icon: 'pspaceend',
        tooltip: pluginName,
        fetch: function(callback) {
            var dom = editor.dom;
            var blocks = editor.selection.getSelectedBlocks();
            var lhv = 0;
            global$1.each(blocks, function(block) {
                if(lhv==0){
                    lhv = dom.getStyle(block,'margin-bottom') ? dom.getStyle(block,'margin-bottom') : 0;
                }
            });
            var items = p_space_end_val.split(' ').map(function(item){
                var text = item;
                var value = item;
                return {
                    type: 'togglemenuitem',
                    text: text,
                    active : lhv==value ? true :false,
                    onAction: function() {
                        doAct(value);
                    }
                };
            });
            callback(items);
        }
    });
    return {
        getMetadata: function () {
            return  {
                name: pluginName,
                url: "https://gitee.com/cls-admin/cls-tinymce-plugin",
            };
        }
    };
  });